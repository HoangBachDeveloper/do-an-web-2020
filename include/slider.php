<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
		<!-- Indicators-->
		<ol class="carousel-indicators">
			<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
			<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
			<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
			<li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
		</ol>
		<div class="carousel-inner">
			<div class="carousel-item item1 active">
				<div class="container">
					<div class="w3l-space-banner">
						<div class="carousel-caption p-lg-5 p-sm-4 p-3">
							<p>Biến văn phòng của bạn thêm phần 
								<span>Sang Trọng</span>  </p>
							<h3 class="font-weight-bold pt-2 pb-lg-5 pb-4">Sự khác biệt về
								<span>Đẳng cấp</span>
							</h3>
						</div>
					</div>
				</div>
			</div>
			<div class="carousel-item item2">
				<div class="container">
					<div class="w3l-space-banner">
						<div class="carousel-caption p-lg-5 p-sm-4 p-3">
							<p>Nội thất
								<span>Tuyệt vời</span></p>
							<h3 class="font-weight-bold pt-2 pb-lg-5 pb-4"> Để Cuộc sống
								<span>tiện nghi</span>
							</h3>
						</div>
					</div>
				</div>
			</div>
			 <div class="carousel-item item3"> 
				<!-- <div class="abc"> -->
				<div class="container">
					<div class="w3l-space-banner">
						<div class="carousel-caption p-lg-5 p-sm-4 p-3">
							<p>Mua sắm 
								<span> Trực Tuyến </span></p>
							<h3 class="font-weight-bold pt-2 pb-lg-5 pb-4"> Chỉ với một cú  
								<span>Click </span>
							</h3>
						</div>
					</div>
				</div>
			</div>
			<div class="carousel-item item4">
				<div class="container">
					<div class="w3l-space-banner">
						<div class="carousel-caption p-lg-5 p-sm-4 p-3">
							<p>Người Việt Nam dùng hàng
								<span>Việt Nam </span></p>
							<h3 class="font-weight-bold pt-2 pb-lg-5 pb-4">Made in 
								<span>Việt nam</span>
							</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
		<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>