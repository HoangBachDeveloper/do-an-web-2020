
	<!-- footer -->
	<footer>
	<div class="join-w3l1 py-sm-5 py-4">
		<div class="container py-xl-4 py-lg-2">
			<div class="row">
				<div class="col-lg-6">
					<div class="join-agile text-left p-4">
						<div class="row">
							<div class="col-sm-7 offer-name">
								<h6>Chúng tôi tồn tại với mong muốn mang tới cho bạn cuộc sống tốt hơn với phương châm:</h6>
								<h4 class="mt-2 mb-3">Luôn luôn lắng nghe - lâu lâu mới hiểu</h4>
								<p>Giảm tới 5% khi mua trực tiếp tại của hàng</p>
							</div>
							<div class="col-sm-5 offerimg-w3l">
								<img src="images/vector-illustration-4487579_1280.png" alt=""  class="img-fluid">
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6 mt-lg-0 mt-5">
					<div class="join-agile text-left p-4">
						<div class="row ">
							<div class="col-sm-7 offer-name">
								<h6>Hãy liên hệ với chúng tôi theo số điện thoại 0373062099 để ngôi nhà của bạn sẽ</h6>
								<h4 class="mt-2 mb-3">Hiện Đại Hơn - Thông Minh Hơn</h4>
								<p>Và được miễn phí giao hàng tới mọi nơi trên lãnh thổ Việt Nam</p>
							</div>
							<div class="col-sm-5 offerimg-w3l">
								<img src="images/article_1544980644_138.png" alt="" class="img-fluid">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
		<div class="footer-top-first">
			<div class="container py-md-5 py-sm-4 py-3">
				<h2 class="footer-top-head-w3l font-weight-bold mb-2">Giới thiệu về chúng tôi</h2>
				<p class="footer-main mb-4">
					Quản trị viên: Nguyễn Hoàng Bách <br>
					Email        : nguyenhoangminhbach@gmail.com <br>
					Faccebook    : https://www.facebook.com/nguyenhoangminhbach<br>
					Điện thoại   : +84 373062099<br>
					Địa chỉ      : Thành phố Vinh - Nghệ An - Việt Nam<br></p>
				<div class="row w3l-grids-footer border-top border-bottom py-sm-4 py-3">
					<div class="col-md-4 offer-footer">
						<div class="row">
							<div class="col-4 icon-fot">
								<i class="fas fa-dolly"></i>
							</div>
							<div class="col-8 text-form-footer">
								<h3>Miễn phí vận chuyển</h3>
								<p>Với đơn hàng trên 1.000.000VNĐ</p>
							</div>
						</div>
					</div>
					<div class="col-md-4 offer-footer my-md-0 my-4">
						<div class="row">
							<div class="col-4 icon-fot">
								<i class="fas fa-shipping-fast"></i>
							</div>
							<div class="col-8 text-form-footer">
								<h3>Giao hàng tận nơi</h3>
								<p>Phạm vi toàn quốc</p>
							</div>
						</div>
					</div>
					<div class="col-md-4 offer-footer">
						<div class="row">
							<div class="col-4 icon-fot">
								<i class="far fa-thumbs-up"></i>
							</div>
							<div class="col-8 text-form-footer">
								<h3>Mặt hàng phong phú</h3>
								<p>Đa dạng mẫu mã</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!-- //footer -->
	<!-- copyright -->
	<div class="copy-right py-3">
		<div class="container">
			<p class="text-center text-white">© 2020 Cửa hàng điện máy. Bản quyền thuộc | Thiết kế bởi
				<a href="https://www.facebook.com/nguyenhoangminhbach"> Nguyễn Hoàng Bách </a>
			</p>
		</div>
	</div>
	<!-- //copyright -->