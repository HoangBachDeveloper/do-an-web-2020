<?php
	session_start();
	if(!isset($_SESSION['dangnhap']))
	{
		header('Location: index.php');
	} 
	if(isset($_GET['login']))
	{
		$dangxuat = $_GET['login'];
	}
	else
	{
	 	$dangxuat = '';
	}
	if($dangxuat =='dangxuat')
	{
		session_destroy();
		header('Location: index.php');
	}
?>
<!DOCTYPE html>
<html lang="en"> 
<head>
	<meta charset="UTF-8">
	<title>Quản Trị</title>
	<link href="../css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
</head>
<body>

	<div align="center">
		<p><strong> Xin chào : <?php echo $_SESSION['dangnhap'] ?></strong><br>
		<a href="?login=dangxuat" class="btn btn-success" >Đăng xuất</a></p>
	</div>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
	  <div class="collapse navbar-collapse" id="navbarNav">
	    <ul class="navbar-nav">
	    	<li class="nav-item active">
	        <a class="nav-link" href="dashbroad.php">Trang chủ<span class="sr-only">(current)</span></a>
	      </li>
	      <li class="nav-item active">
	        <a class="nav-link" href="xulydonhang.php">Đơn hàng <span class="sr-only">(current)</span></a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="xulydanhmuc.php">Danh mục Sản Phẩm</a>
	      </li>
	       <li class="nav-item">
	        <a class="nav-link" href="xulydanhmucbaiviet.php">Danh mục Bài viết</a>
	      </li>
	       <li class="nav-item">
	        <a class="nav-link" href="xulybaiviet.php">Bài viết</a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="xulysanpham.php">Sản phẩm</a>
	      </li>
	       <li class="nav-item">
	        <a class="nav-link" href="xulykhachhang.php">Khách hàng</a>
	      </li>
	       </li>
	       <li class="nav-item">
	        <a class="nav-link" href="quantri.php">Nhân Sự</a>
	      </li>
	    </ul>
	  </div>
	</nav><br><br>
</body>
</html>