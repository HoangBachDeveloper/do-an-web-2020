-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th7 25, 2020 lúc 03:32 AM
-- Phiên bản máy phục vụ: 10.4.11-MariaDB
-- Phiên bản PHP: 7.2.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `banhangdienmay`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `admin_id` int(11) NOT NULL,
  `admin_name` varchar(100) NOT NULL,
  `admin_email` varchar(50) NOT NULL,
  `admin_pass` varchar(100) NOT NULL,
  `admin_type` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `tbl_admin`
--

INSERT INTO `tbl_admin` (`admin_id`, `admin_name`, `admin_email`, `admin_pass`, `admin_type`) VALUES
(1, 'Hoàng Bách', 'admin', '21232f297a57a5a743894a0e4a801fc3', 1),
(11, 'Maiphan', 'mai', '2b28587f6d880ea9fc27c6c48fe3f1eb', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_baiviet`
--

CREATE TABLE `tbl_baiviet` (
  `baiviet_id` int(11) NOT NULL,
  `tenbaiviet` varchar(100) NOT NULL,
  `tomtat` text NOT NULL,
  `noidung` text NOT NULL,
  `danhmuc_tin_id` int(11) NOT NULL,
  `baiviet_image` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `tbl_baiviet`
--

INSERT INTO `tbl_baiviet` (`baiviet_id`, `tenbaiviet`, `tomtat`, `noidung`, `danhmuc_tin_id`, `baiviet_image`) VALUES
(5, 'Apple giảm sốc nhiều sản phẩm iPhone, iPad đến 5 triệu, xem ngay có chiếc máy mà bạn đã để ý không, ', 'Hàng loạt iPhone, iPad được nhà Apple khuyến mãi linh đình, ưu đãi giảm giá lên đến 5 triệu đồng', 'Xem chi tiết tại: https://www.thegioididong.com/tin-tuc/iphone-ipad-giam-gia-1258893', 4, 'm3.jpg'),
(6, 'Ra mắt iPhone 11, Apple của ngày xưa đã chết', 'Sự kiện ra mắt iPhone 11 là minh chứng cho thấy ai rồi cũng phải thay đổi, kể cả là công ty công nghệ hàng đầu như Apple.', 'Sự kiện ra mắt iPhone 11 là minh chứng cho thấy ai rồi cũng phải thay đổi, kể cả là công ty công nghệ hàng đầu như Apple.\r\nGần cuối sự kiện ra mắt iPhone 2019, có một chi tiết khiến mạch theo dõi của tôi dừng lại. Đó không phải là một tính năng quá hấp dẫn, mà là một hình ảnh trình chiếu. Trên màn hình, đại diện Apple nói về chiếc iPhone 11, và đằng sau là một loạt tính năng của máy được liệt kê ra.\r\n\r\nĐiều khiến tôi chú ý chính là sự lộn xộn của tấm hình này. Có khoảng hơn 10 tính năng được liệt kê ra, mỗi thứ một phông chữ, một màu sắc, một hình ảnh khác nhau. Nó khác hoàn toàn với những ảnh trình chiếu thông thường của Apple, vốn chỉ có tông màu đen và một vài dòng chữ đơn giản.\r\n', 4, 'iphone-11-pro-max-black-400x460.png'),
(7, 'Apple ra mắt tai nghe AirPods Pro chống ồn', 'Mẫu tai nghe truewireless chống ồn đầu tiên của Apple sẽ được bán từ 31/10 với giá 250 USD.', 'AirPods Pro có thiết kế được cải tiến nhiều so với trước đây với phần thân chứa pin ngắn hơn và được làm xiên thay vì thẳng, bên cạnh đó tai nghe dạng in ear với đệm silicon giúp đem lại chất lượng âm thanh tốt hơn, đồng thời tích hợp tính năng chống ồn chủ động. Ngoài ra AirPods Pro còn có hệ thống ven điều áp giúp mang lại sự dễ chịu cho người dùng thường thấy trên các tai nghe in ear.\r\n\r\nSản phẩm sử dụng chip H1 do Apple thiết kế với 10 nhân xử lý âm thanh. Apple cho biết con chip có khả năng xử lý audio độ trễ thấp, chống ồn chủ động thời gian thực, cho chất lượng âm thanh cao cấp adaptive, hỗ trợ Siri nhanh chóng.\r\n\r\nTính năng đáng chú ý nhất của sản phẩm lần này là chống ồn chủ động với 2 micro ở trong và ngoài tai có khả năng xử lý tín hiệu âm thanh ở bên ngoài lên đến 200 lần mỗi giây. Mỗi tai nghe sẽ xử lý tiếng ồn độc lập giúp mang lại khả năng chống ồn tốt nhất. Bên cạnh khả năng chống ồn, model này có thêm chế độ Transparency cho phép người dùng có thể nghe được âm thanh bên ngoài trong khi đang nghe nhạc hay gọi điện. Tính năng này giống với chế độ Ambience Sound của Sony. Người dùng có thể chuyển đổi các chế độ, chơi nhạc hay nhận cuộc gọi bằng cách bóp vào phần thân của tai nghe, Apple sử dụng cảm biến lực để điều khiển tai nghe mới của mình thay vì chạm như trước.  \r\n\r\nChất lượng âm thanh được nâng cấp khi có thêm tính năng Adaptive EQ tự động điều chỉnh âm thanh của từng bài hát. Apple cũng trang bị một bộ khuếch đại cho AirPods Pro giúp tái tạo âm thanh tốt hơn và giảm đi việc bị méo tiếng.\r\n\r\nAirPods Pro có thời lượng pin 5 tiếng khi nghe nhạc. Nếu mở chế độ chống ồn chủ động thời gian sử dụng giảm xuống còn 4,5 tiếng và 3,5 tiếng nếu sử dụng để gọi điện thoại. Hộp đựng AirPods Pro có tích hợp pin giúp nâng tổng thời gian sử dụng lên 24 tiếng nếu dùng nghe nhạc và 18 tiếng gọi điện. Apple tích hợp sạc không dây chuẩn Qi cho model của mình, sản phẩm vẫn sử dụng dây sạc lightning. AirPods Pro chỉ có duy nhất màu trắng thay vì nhiều màu như tin đồn trước đó.', 6, 'AirPods-Pro-copy-2933-1572284801.jpg'),
(8, 'Samsung ra mắt bản kính thực tế ảo Gear VR mới', 'Samsung và Oculus hôm nay vừa ra mắt một phiên bản kính thực tế ảo Gear VR mới có giá chỉ 99$, rẻ hơn nhiều so với chiếc Gear VR Innovator dành cho lập trình viên vốn có giá 199$ khi ra mắt hồi tháng 3 năm nay.', 'Phiên bản Gear VR mới có trọng lượng 310g, nhẹ hơn 22% so với đời trước về có đi kèm các tấm đệm để giúp việc đeo kính trở nên dễ chịu hơn. Touchpad điều khiển trên sản phẩm cũng được cải thiện để tăng độ chính xác trong quá trình chơi game, xem phim hay trải nghiệm nội dung trong không gian ảo.\r\nKính thực tế ảo Gear VR mới sẽ bắt đầu bán ra ở Mỹ ngay trong tháng 11 năm nay và sau đó sẽ dần cập bến các quốc gia khác. Nó có khay đặt vừa hầu hết các điện thoại Samsung mới, bao gồm S6, S6 Edge, Note 5 và S6 Edge+, và vẫn dùng kết nối microUSB như trước.', 6, 'kinh-thuc-te-ao-samsung-gear-vr-r322.jpeg'),
(9, 'Tìm hiểu về mẫu máy giặt thế hệ mới của Samsung vừa được ra mắt gần đây', 'Thương hiệu điện tử, điện lạnh hàng đầu thế giới - Samsung vừa chính thức cho ra mắt một mẫu sản phẩm thế hệ mới thuộc dòng Addwash, máy giặt Samsung 10Kg WW10K54E0UX, một sản phẩm được giới chuyên gia đánh giá rất cao về các tính năng sử dụng. Vậy hãy cùng siêu thị Điện Máy - Nội Thất Chợ Lớn tìm hiểu xem mẫu máy giặt này có những điểm gì nổi bật và có xứng đáng để chúng ta \"rước về\" hay không nhé!', '1. Thiết kế thời thượng\r\nXét về tổng thể sản phẩm thì mẫu máy giặt 10Kg này sở hữu những đường nét thiết kế đơn giản, thông minh mang lại sự tinh tế, hiện đại cho thiết bị. Phần nút điều khiển được thiết kế thân thiện, chú thích rõ ràng giúp cho người dùng thuận tiện hơn trong việc làm quen và sử dụng máy giặt.', 5, 'image001(1).png'),
(10, 'TCL ra mắt 3 dòng máy giặt mới T-Clean tại thị trường Việt Nam', 'TCL vừa ra mắt tại thị trường Việt Nam bộ sưu tập máy giặt mới mang tên T-Clean gồm ba dòng K(King) series, M(Magic) series và B(Bass) series với ba model K08, M03 và B302. Hãy cùng Điện Máy Chợ Lớn tìm hiểu bộ sưu tập máy giặt mới này nhé!', 'Bộ sưu tập máy giặt T-Clean gồm ba dòng K, M và B phù hợp với đối tượng gia đình phổ thông, trung và cao cấp với hai tiêu chí là Chống bẩn, Bảo vệ quần áo – “Clear Inside, Clean Outside”.\r\n\r\nDòng máy giặt B302 được TCL thiết kế với nắp kính cường lực thân thiện, tăng độ bền trong quá trình sử dụng. Thiết kế lồng giặt bên trong độc đáo được trang bị cấu trúc tinh thể tổ ong xoay 360 độ, tạo ra một lớp màng nước nơi quần áo có thể nhẹ nhàng xẹp xuống và vết bẩn có thể được loại bỏ một cách hiệu quả. Lồng giặt còn có lỗ thoát nước siêu nhỏ, cố định quần áo hiệu quả hơn, giảm thiểu nguy cơ sợi vải vướng vào các lỗ thoát nước gây tổn hại đến quần áo. Đặc biệt, tính năng độc đáo của mâm giặt là có thể tháo rời để người dùng dễ dàng vệ sinh cặn bẩn đọng lại sau nhiều lần giặt, đảm bảo nguồn nước luôn luôn được sạch sẽ.', 5, 'image001(110).jpg'),
(11, 'Ngày xưa mà chúng ta sử dụng dự án tủ lạnh của Albert Einstein và Leo Szilard, có khi tầng ozone đã ', 'Đây là dự án ít người biết tới của Albert Einstein, và ông quyết tâm bỏ công sức chế tạo tủ lạnh khi đọc được một mẩu tin quặn lòng trên báo.', 'Lược dịch từ đoạn trích trong cuốn sách của Sam Kean, tựa đề  “Lời trăn trối của Caesar - Giải mã bí mật trong không khí quanh ta”.\r\n\r\nCó một sự thật nhiều người biết là việc phát triển vũ khí hạt nhân đã dọn đường cho nhân loại có được thiết bị máy tính điện tử đầu tiên. Nhưng ít ai biết rằng, theo một cách nào đó, công nghệ tủ lạnh cũng lại cho phép chúng ta có được … bom nguyên tử.\r\n\r\nMột buổi sáng năm 1926, Albert Einstein suýt nghẹn món trứng điểm tâm khi đọc được tin dữ trên báo: vài đêm trước, một gia đình ở Berlin đã chết ngạt khi tủ lạnh hỏng và khí gas độc tràn ra ngoài. Trong đau đớn, nhà vật lý học bốn mươi bảy tuổi tức tốc đánh điện cho người bạn trẻ của mình, nhà phát minh, nhà khoa học Leo Szilard. “Chắc chắn phải có cách nào đó tốt hơn chứ”, Einstein nói.', 1, '3761_sup2ljpg.jpg');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_category`
--

CREATE TABLE `tbl_category` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `tbl_category`
--

INSERT INTO `tbl_category` (`category_id`, `category_name`) VALUES
(11, 'Máy Lạnh'),
(12, 'Tủ Lạnh'),
(13, 'Máy Giặt'),
(14, 'Điện Thoại'),
(15, 'TiVi '),
(19, 'Phụ Kiện Điện Thoại'),
(20, 'Cá rô phi');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_danhmuc_tin`
--

CREATE TABLE `tbl_danhmuc_tin` (
  `danhmuc_tin_id` int(11) NOT NULL,
  `tendanhmuc` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `tbl_danhmuc_tin`
--

INSERT INTO `tbl_danhmuc_tin` (`danhmuc_tin_id`, `tendanhmuc`) VALUES
(1, 'Bí kíp giữ hoa quả tươi ngon'),
(2, 'Giải nhiệt mùa hè'),
(3, 'Rạp chiếu phim tại gia'),
(4, 'Chạy đua cùng công nghệ Smart Phone'),
(5, 'Nói không với giặt bằng tay '),
(6, 'Phụ kiện cho dế yêu');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_donhang`
--

CREATE TABLE `tbl_donhang` (
  `donhang_id` int(11) NOT NULL,
  `sanpham_id` int(11) NOT NULL,
  `donhang_soluong` int(11) NOT NULL,
  `donhang_mahang` varchar(50) NOT NULL,
  `khachhang_id` int(11) NOT NULL,
  `ngaythang` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `tinhtrang` int(11) NOT NULL,
  `huydon` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `tbl_donhang`
--

INSERT INTO `tbl_donhang` (`donhang_id`, `sanpham_id`, `donhang_soluong`, `donhang_mahang`, `khachhang_id`, `ngaythang`, `tinhtrang`, `huydon`) VALUES
(61, 1, 1, '3788', 26, '2020-06-06 04:54:58', 1, 2),
(62, 3, 1, '3788', 26, '2020-06-06 04:54:58', 1, 2),
(63, 4, 1, '3788', 26, '2020-06-06 04:54:58', 1, 2),
(64, 20, 1, '6386', 26, '2020-06-11 02:38:09', 0, 2),
(65, 14, 1, '6386', 26, '2020-06-11 02:38:09', 0, 2),
(66, 21, 2, '463', 26, '2020-06-17 05:09:07', 0, 2),
(67, 8, 1, '463', 26, '2020-06-17 05:09:07', 0, 2),
(68, 4, 1, '463', 26, '2020-06-17 05:09:07', 0, 2),
(69, 20, 10, '5679', 26, '2020-07-24 07:57:22', 0, 2),
(70, 21, 1, '2986', 34, '2020-06-17 05:09:20', 1, 0),
(71, 1, 3, '7826', 26, '2020-07-24 07:58:21', 1, 0),
(72, 20, 1, '7826', 26, '2020-07-24 07:58:21', 1, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_giaodich`
--

CREATE TABLE `tbl_giaodich` (
  `giaodich_id` int(11) NOT NULL,
  `sanpham_id` int(11) NOT NULL,
  `soluong` int(11) NOT NULL,
  `magiaodich` varchar(50) NOT NULL,
  `ngaythang` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `khachhang_id` int(11) NOT NULL,
  `tinhtrangdon` int(11) NOT NULL DEFAULT 0,
  `huydon` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `tbl_giaodich`
--

INSERT INTO `tbl_giaodich` (`giaodich_id`, `sanpham_id`, `soluong`, `magiaodich`, `ngaythang`, `khachhang_id`, `tinhtrangdon`, `huydon`) VALUES
(41, 20, 1, '6918', '2020-06-06 03:42:21', 29, 0, 1),
(42, 19, 1, '9087', '2020-06-06 04:08:38', 29, 0, 1),
(43, 21, 1, '7763', '2020-06-06 04:10:51', 31, 0, 0),
(44, 1, 1, '3788', '2020-06-06 04:36:41', 26, 1, 2),
(45, 3, 1, '3788', '2020-06-06 04:36:41', 26, 1, 2),
(46, 4, 1, '3788', '2020-06-06 04:36:41', 26, 1, 2),
(47, 20, 1, '6386', '2020-06-11 02:38:09', 26, 0, 2),
(48, 14, 1, '6386', '2020-06-11 02:38:09', 26, 0, 2),
(49, 21, 2, '463', '2020-06-17 05:09:07', 26, 0, 2),
(50, 8, 1, '463', '2020-06-17 05:09:07', 26, 0, 2),
(51, 4, 1, '463', '2020-06-17 05:09:07', 26, 0, 2),
(52, 20, 10, '5679', '2020-07-24 07:57:22', 26, 0, 2),
(53, 21, 1, '2986', '2020-06-17 05:09:20', 34, 1, 0),
(54, 1, 3, '7826', '2020-07-24 07:58:21', 26, 1, 0),
(55, 20, 1, '7826', '2020-07-24 07:58:21', 26, 1, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_giohang`
--

CREATE TABLE `tbl_giohang` (
  `giohang_id` int(11) NOT NULL,
  `giohang_tensanpham` varchar(100) NOT NULL,
  `sanpham_id` int(11) NOT NULL,
  `giohang_hinhanh` varchar(50) NOT NULL,
  `giohang_gia` varchar(50) NOT NULL,
  `giohang_soluong` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `tbl_giohang`
--

INSERT INTO `tbl_giohang` (`giohang_id`, `giohang_tensanpham`, `sanpham_id`, `giohang_hinhanh`, `giohang_gia`, `giohang_soluong`) VALUES
(95, 'Máy giặt Toshiba Inverter 8.5 Kg TW-BH95S2V WK', 5, 'product_13794_4.png', '7500000', 1),
(96, 'Tủ lạnh Samsung Inverter 617 lít RS64R5301B4/SV', 15, 'product_14935_1.png', '2800000', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_khachhang`
--

CREATE TABLE `tbl_khachhang` (
  `khachhang_id` int(11) NOT NULL,
  `khachhang_name` varchar(100) NOT NULL,
  `khachhang_phone` varchar(20) NOT NULL,
  `khachhang_add` varchar(200) NOT NULL,
  `khachhang_note` varchar(100) NOT NULL,
  `khachhang_email` varchar(100) NOT NULL,
  `khachhang_matkhau` varchar(100) NOT NULL,
  `khachhang_giaohang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `tbl_khachhang`
--

INSERT INTO `tbl_khachhang` (`khachhang_id`, `khachhang_name`, `khachhang_phone`, `khachhang_add`, `khachhang_note`, `khachhang_email`, `khachhang_matkhau`, `khachhang_giaohang`) VALUES
(26, 'Bách Hoàng', '123456789', 'Vinh', 'Giao hàng nhanh nhé!										\r\n										', 'bachhoang@gmail.com', '202cb962ac59075b964b07152d234b70', 1),
(31, 'Hồng Nguyễn', '123456789', 'Vinh ', '											\r\n										', 'hong@gmai.com', '202cb962ac59075b964b07152d234b70', 1),
(32, 'Mai', '123456', 'Vinh', 'sd', 'mai@gmail.com', '123', 1),
(33, 'Bách ', '123456789', 'Vinh', 'Giao', 'bach@gmail.com', '202cb962ac59075b964b07152d234b70', 0),
(34, 'Mai', '123456', 'Vinh', '											\r\n										', 'mai@gmail.com', '202cb962ac59075b964b07152d234b70', 1),
(35, 'Bách', '123456789', 'Vinh', 'KHông có gì cả', 'bach1@gmail.com', '202cb962ac59075b964b07152d234b70', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_slider`
--

CREATE TABLE `tbl_slider` (
  `slider_id` int(11) NOT NULL,
  `slider_image` varchar(100) NOT NULL,
  `slider_caption` text NOT NULL,
  `slider_active` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `tbl_slider`
--

INSERT INTO `tbl_slider` (`slider_id`, `slider_image`, `slider_caption`, `slider_active`) VALUES
(1, 'b2.jpg', 'Slider Khuyến Mãi', 1),
(2, 'b3.jpg', 'Slider 50%', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tlb_sanpham`
--

CREATE TABLE `tlb_sanpham` (
  `sanpham_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `sanpham_name` varchar(255) NOT NULL,
  `sanpham_gia` varchar(50) NOT NULL,
  `sanpham_giakm` varchar(100) NOT NULL,
  `sanpham_active` int(11) NOT NULL DEFAULT 1,
  `sanpham_hot` int(11) NOT NULL,
  `sanpham_soluong` int(11) NOT NULL,
  `sanpham_imaage` varchar(50) NOT NULL,
  `sanpham_chitiet` text NOT NULL,
  `sanpham_mota` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `tlb_sanpham`
--

INSERT INTO `tlb_sanpham` (`sanpham_id`, `category_id`, `sanpham_name`, `sanpham_gia`, `sanpham_giakm`, `sanpham_active`, `sanpham_hot`, `sanpham_soluong`, `sanpham_imaage`, `sanpham_chitiet`, `sanpham_mota`) VALUES
(1, 14, 'SamSung Galaxy Note 9', '800000', '7999000', 1, 0, 10, 'samsung-galaxy-note-9-black-400x460-400x460.png', '- Màn hình: Super AMOLED, 6.4″\r\nQuad HD+ (2K+)\r\n\r\n- Hệ điều hành: Android 8.1 (Oreo)\r\n\r\nCamera sau: Chính 12 MP & Phụ 12 MP\r\nCamera trước: 8 MP\r\n\r\n- CPU: Exynos 9810 8 nhân 64-bit\r\nRAM: 6 GB\r\n\r\n- Bộ nhớ trong: 128 GB\r\n\r\n- Thẻ nhớ: MicroSD\r\n\r\n- Hỗ trợ tối đa 512 GB\r\n\r\n- Thẻ SIM: 2 SIM Nano (SIM 2 chung khe thẻ nhớ)\r\n\r\n- Hỗ trợ 4G\r\n', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'),
(2, 13, 'Máy giặt Panasonic Inverter 10 Kg NA-V10FG1WVT', '1800000', '17900000', 1, 1, 10, 'may-giat-panasonic-na-f85a4grv-1-300x300.png', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'),
(3, 14, 'Iphone 11 ProMax', '30000000', '29900000', 1, 1, 10, 'iphone-11-pro-max-black-400x460.png', '', ''),
(4, 14, 'BPhone B86', '9000000', '8900000', 1, 1, 10, 'bphone-color-mb.png', '', ''),
(5, 13, 'Máy giặt Toshiba Inverter 8.5 Kg TW-BH95S2V WK', '7990000', '7500000', 1, 0, 10, 'product_13794_4.png', '', ''),
(6, 13, 'Máy giặt sấy LG Inverter 21 kg F2721HTTV', '45000000', '44999000', 1, 1, 10, 'may-giat-samsung-wd17j7825kp-sv-300x300.png', '', ''),
(7, 15, 'Smart Tivi Samsung 4K 65 inch', '25000000', '24900000', 1, 1, 10, '464_samsung_ua65tu7000_tu7000_1_org.jpg', '', ''),
(8, 15, 'Smart Tivi Samsung 4K 50 inch ', '10000000', '9500000', 1, 1, 10, 'samsung-ua50ru7100-550x340.jpg', '', ''),
(9, 15, 'Android Tivi TCL 50 inch 50P715', '13000000', '12500000', 1, 0, 10, 'product_16160_8.png', '', ''),
(10, 11, 'Quạt điều hòa Honeywell TC10PM', '1500000', '1300000', 1, 1, 10, 'may-lam-mat-khong-khi-honeywell-es800-1.jpg', '', ''),
(11, 11, 'Điều hòa Kangaroo 1 HP KGAC09CN', '7000000', '6900000', 1, 1, 10, 'kangaroo-kgac09cn-1-550x160.jpg', '', ''),
(12, 11, 'Điều hòa 1 chiều Casper 12000BTUSC12TL22', '6000000', '6000000', 1, 1, 10, 'dieu-hoa-sc09tl22.jpg', '', ''),
(13, 12, 'Tủ lạnh Toshiba Inverter 194 lít GR-A25VM (UKG)', '6500000', '6000000', 1, 0, 10, 'toshiba-gr-a25vm-ukg-13-300x300.jpg', '', ''),
(14, 12, 'Tủ lạnh Electrolux 85 lít EUM0900SA', '3000000', '2800000', 1, 1, 10, 'tu-lanh-electrolux-eum0900sa-1-300x300.jpg', '', ''),
(15, 12, 'Tủ lạnh Samsung Inverter 617 lít RS64R5301B4/SV', '3000000', '2800000', 1, 1, 10, 'product_14935_1.png', '', ''),
(19, 19, 'Tai nghe chụp tai Gaming Logitech G431 7.1 Đen Xanh', '3000000', '2900000', 1, 0, 10, 'tai.jpg', '', ''),
(20, 19, 'Tai nghe Game thủ V5000', '200000', '176000', 1, 0, 10, 'game.jpg', '', ''),
(21, 19, ' Headphone Gaming Z18', '400000', '350000', 1, 0, 10, 'unnamed.jpg', 'Xuất xứ Trung Quốc\r\nCó Mic Âm thanh chất lượng Dây loa dài >1.5 mét\r\nTai nghe Gaming Headphone With Mic for Game Z18\r\nĐặc điểm Tai nghe dành cho quán GAME - Âm thanh hay, to - bass ấm\r\nDây cực to không sợ bị đứt, bị soắn dây\r\nVolume liền tai không sợ bị bung. Điểm đặc biệt là khi mở Volume nhỏ nhất vẫn còn nghe được tiếng (những phone thường mở nhỏ là im re luôn)\r\nViền tai bằng CAO SU - đeo mùa hè không sợ nóng và không bị bung như viền vải nên bền hơn', 'Đệm tai giữ kín âmMicrophone chất lượngÂm bass sâuCổng kết nối: 2 jack 3.5mm cho mic + loa, 1 jack USB cho LedDây bọc vải dù, dài 2.1mĐèn led RGB đổi màu liên tụcbảo hành 6 tháng '),
(22, 20, 'Cá vàng', '123', '123', 1, 0, 3, '3761_sup2ljpg.jpg', 'gfgf', 'fdfdf');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Chỉ mục cho bảng `tbl_baiviet`
--
ALTER TABLE `tbl_baiviet`
  ADD PRIMARY KEY (`baiviet_id`);

--
-- Chỉ mục cho bảng `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`category_id`);

--
-- Chỉ mục cho bảng `tbl_danhmuc_tin`
--
ALTER TABLE `tbl_danhmuc_tin`
  ADD PRIMARY KEY (`danhmuc_tin_id`);

--
-- Chỉ mục cho bảng `tbl_donhang`
--
ALTER TABLE `tbl_donhang`
  ADD PRIMARY KEY (`donhang_id`);

--
-- Chỉ mục cho bảng `tbl_giaodich`
--
ALTER TABLE `tbl_giaodich`
  ADD PRIMARY KEY (`giaodich_id`);

--
-- Chỉ mục cho bảng `tbl_giohang`
--
ALTER TABLE `tbl_giohang`
  ADD PRIMARY KEY (`giohang_id`);

--
-- Chỉ mục cho bảng `tbl_khachhang`
--
ALTER TABLE `tbl_khachhang`
  ADD PRIMARY KEY (`khachhang_id`);

--
-- Chỉ mục cho bảng `tbl_slider`
--
ALTER TABLE `tbl_slider`
  ADD PRIMARY KEY (`slider_id`);

--
-- Chỉ mục cho bảng `tlb_sanpham`
--
ALTER TABLE `tlb_sanpham`
  ADD PRIMARY KEY (`sanpham_id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT cho bảng `tbl_baiviet`
--
ALTER TABLE `tbl_baiviet`
  MODIFY `baiviet_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT cho bảng `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT cho bảng `tbl_danhmuc_tin`
--
ALTER TABLE `tbl_danhmuc_tin`
  MODIFY `danhmuc_tin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `tbl_donhang`
--
ALTER TABLE `tbl_donhang`
  MODIFY `donhang_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT cho bảng `tbl_giaodich`
--
ALTER TABLE `tbl_giaodich`
  MODIFY `giaodich_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT cho bảng `tbl_giohang`
--
ALTER TABLE `tbl_giohang`
  MODIFY `giohang_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;

--
-- AUTO_INCREMENT cho bảng `tbl_khachhang`
--
ALTER TABLE `tbl_khachhang`
  MODIFY `khachhang_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT cho bảng `tbl_slider`
--
ALTER TABLE `tbl_slider`
  MODIFY `slider_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `tlb_sanpham`
--
ALTER TABLE `tlb_sanpham`
  MODIFY `sanpham_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
